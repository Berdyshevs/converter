package ru.sberbank.converter.net;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpLoader {

    private static final String TAG = HttpLoader.class.getSimpleName();
    private HttpLoadCallback httpLoadCallback;

    public void getData(String url, HttpLoadCallback httpLoadCallback) {
        this.httpLoadCallback = httpLoadCallback;
        HttpLoaderTask httpLoaderTask = new HttpLoaderTask();
        httpLoaderTask.execute(url);
    }

    public interface HttpLoadCallback {
        void loadComplete(String result);
        void loadError();
    }

    @SuppressLint("StaticFieldLeak")
    protected class HttpLoaderTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return loadData(urls[0]);
        }

        @Override
        protected void onPostExecute(@NonNull String result) {
            if (result != null) {
                httpLoadCallback.loadComplete(result);
            } else {
                httpLoadCallback.loadError();
            }
        }

        public String loadData(String requestUrl) {
            String response = null;
            InputStream inputStream = null;
            HttpURLConnection connection = null;

            try {
                URL url = new URL(requestUrl);
                connection = (HttpURLConnection) url.openConnection();
                connection.setConnectTimeout(3000);
                connection.setReadTimeout(3000);
                connection.setRequestMethod("GET");
                connection.connect();
                inputStream = new BufferedInputStream(connection.getInputStream());

                if (inputStream != null) {
                    response = convertStreamToString(inputStream);
                }
            } catch (IOException e) {
                Log.e(TAG, "IOException: " + e.getMessage());
            } finally {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                if (connection != null) {
                    connection.disconnect();
                }
            }

            return response;
        }

        private String convertStreamToString(InputStream inputStream) {
            BufferedReader bufferedReader = null;
            try {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "windows-1251"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            try {
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line).append('\n');
                }
            } catch (IOException e) {
                Log.e(TAG, "IOException: " + e.getMessage());
            } finally {

                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return stringBuilder.toString();

        }
    }
}