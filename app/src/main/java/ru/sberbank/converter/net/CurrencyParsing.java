package ru.sberbank.converter.net;

import org.simpleframework.xml.core.Persister;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ru.sberbank.converter.model.Currency;
import ru.sberbank.converter.model.ValCurs;
import ru.sberbank.converter.model.Valute;

public class CurrencyParsing {

    public List<Currency> parse(String content) {
        final Reader reader = new StringReader(content);
        try {
            final Persister persister = new Persister();
            ValCurs valCurs = persister.read(ValCurs.class, reader, false);
            List<Valute> valute = valCurs.getListValutes();
            return convertToCurrency(valute);
        } catch (Exception e) {
            return null;
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private Currency setCurrency(Valute valute) {
        Currency currency = new Currency();
        currency.setId(valute.getId());
        currency.setNumCode(valute.getNumCode());
        currency.setCharCode(valute.getCharCode());
        currency.setNominal(valute.getNominal());
        currency.setName(valute.getName());
        currency.setValue(parseFloat(valute.getValue()));
        return currency;
    }

    /**
    Метод добавляет валюту Российский рубль в список
     */
    private Currency setCurrencyRub() {
        Currency currency = new Currency();
        currency.setId("1009");
        currency.setNumCode("643");
        currency.setCharCode("RUB");
        currency.setNominal(1);
        currency.setName("Российский рубль");
        currency.setValue(1);
        return currency;
    }

    private List<Currency> convertToCurrency(List<Valute> valutes) {
        final List<Currency> currencies = new ArrayList<>();
        currencies.add(setCurrencyRub());
        for (Valute valute : valutes) {
            currencies.add(setCurrency(valute));
        }
        return currencies;
    }

    private float parseFloat(String numberStr) {
        final NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);
        try {
            final Number number = format.parse(numberStr);
            return number.floatValue();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return -1f;
    }
}
