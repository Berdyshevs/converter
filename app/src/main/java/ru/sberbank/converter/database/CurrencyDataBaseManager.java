package ru.sberbank.converter.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import java.util.ArrayList;
import java.util.List;
import ru.sberbank.converter.model.Currency;

public class CurrencyDataBaseManager {
    private CurrencyDataBaseHelper dataBaseHelper;

    public CurrencyDataBaseManager(@NonNull CurrencyDataBaseHelper dbHelper) {
        this.dataBaseHelper = dbHelper;
    }

    @NonNull
    public List<Currency> getListCurrency() {
        final SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
        final List<Currency> currencies = new ArrayList<>();
        final Cursor cursor = db.query(
                CurrencyDataBaseHelper.CURRENCY_TABLE, null, null, null, null, null, null);
        try {
            int idColumnIndex = cursor.getColumnIndex(CurrencyDataBaseHelper.ID_COLUMN);
            int charCodeColumnIndex = cursor.getColumnIndex(CurrencyDataBaseHelper.CHARCODE_COLUMN);
            int numCodeColumnIndex = cursor.getColumnIndex(CurrencyDataBaseHelper.NUMCODE_COLUMN);
            int nominalColumnIndex = cursor.getColumnIndex(CurrencyDataBaseHelper.NOMINAL_COLUMN);
            int nameColumnIndex = cursor.getColumnIndex(CurrencyDataBaseHelper.NAME_COLUMN);
            int valueColumnIndex = cursor.getColumnIndex(CurrencyDataBaseHelper.VALUE_COLUMN);
            if (cursor.moveToFirst()) {
                do {
                    final Currency currency = new Currency();
                    currency.setId(cursor.getString(idColumnIndex));
                    currency.setCharCode(cursor.getString(charCodeColumnIndex));
                    currency.setNumCode(cursor.getString(numCodeColumnIndex));
                    currency.setNominal(cursor.getInt(nominalColumnIndex));
                    currency.setName(cursor.getString(nameColumnIndex));
                    currency.setValue(cursor.getFloat(valueColumnIndex));
                    currencies.add(currency);
                } while (cursor.moveToNext());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.close();
        }
        return currencies;
    }

    public boolean isEmpty() {
        final SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
        final Cursor cursor = db.query(CurrencyDataBaseHelper.CURRENCY_TABLE, null, null, null, null, null, null);
        if (cursor.moveToNext()) {
            cursor.close();
            db.close();
            return false;
        }
        else{
            cursor.close();
            db.close();
            return true;
        }
    }

    public void setListCurrency(@NonNull List<Currency> currencies) {
        if (!currencies.isEmpty()) {
            final SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
            db.beginTransaction();
            for (Currency currency : currencies) {
                ContentValues cv = new ContentValues(6);
                cv.put(CurrencyDataBaseHelper.ID_COLUMN, currency.getId());
                cv.put(CurrencyDataBaseHelper.NUMCODE_COLUMN, currency.getNumCode());
                cv.put(CurrencyDataBaseHelper.CHARCODE_COLUMN, currency.getCharCode());
                cv.put(CurrencyDataBaseHelper.NOMINAL_COLUMN, currency.getNominal());
                cv.put(CurrencyDataBaseHelper.NAME_COLUMN, currency.getName());
                cv.put(CurrencyDataBaseHelper.VALUE_COLUMN, currency.getValue());
                db.replace(CurrencyDataBaseHelper.CURRENCY_TABLE, null, cv);
            }
            db.setTransactionSuccessful();
            db.endTransaction();
            db.close();
        }
    }
}