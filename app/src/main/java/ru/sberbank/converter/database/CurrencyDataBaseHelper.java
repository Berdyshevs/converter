package ru.sberbank.converter.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class CurrencyDataBaseHelper extends SQLiteOpenHelper {
    static final int DATA_BASE_VERSION = 1;
    static final String DATA_BASE_NAME = "currency";
    static final String CURRENCY_TABLE = "currencies";
    static final String ID_COLUMN = "id";
    static final String NUMCODE_COLUMN = "numcode";
    static final String CHARCODE_COLUMN = "charcode";
    static final String NOMINAL_COLUMN = "nominal";
    static final String NAME_COLUMN = "name";
    static final String VALUE_COLUMN = "value";

    public CurrencyDataBaseHelper(Context context) {
        super(context, DATA_BASE_NAME, null, DATA_BASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table "
                + CURRENCY_TABLE + " ("
                + ID_COLUMN + " VARCHAR primary key,"
                + NUMCODE_COLUMN + " VARCHAR(3),"
                + CHARCODE_COLUMN + " VARCHAR(3),"
                + NOMINAL_COLUMN + " INTEGER,"
                + NAME_COLUMN + " VARCHAR,"
                + VALUE_COLUMN + " DECIMAL" + ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}