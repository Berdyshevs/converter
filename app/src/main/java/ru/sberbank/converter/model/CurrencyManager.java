package ru.sberbank.converter.model;

import android.content.Context;
import android.widget.Toast;

import ru.sberbank.converter.database.CurrencyDataBaseHelper;
import ru.sberbank.converter.database.CurrencyDataBaseManager;
import ru.sberbank.converter.net.CurrencyParsing;
import ru.sberbank.converter.net.HttpLoader;

public class CurrencyManager {
    private static final String URL = "http://www.cbr.ru/scripts/xml_daily.asp";

    public void getDataCurrency(final Context context) {
        final HttpLoader httpLoader = new HttpLoader();
        final CurrencyParsing currencyParsing = new CurrencyParsing();
        final CurrencyDataBaseHelper dbHelper = new CurrencyDataBaseHelper(context);
        final CurrencyDataBaseManager currencyDataBaseManager = new CurrencyDataBaseManager(dbHelper);
        httpLoader.getData(URL, new HttpLoader.HttpLoadCallback() {
            @Override
            public void loadComplete(String result) {
                currencyDataBaseManager.setListCurrency(currencyParsing.parse(result));
            }

            @Override
            public void loadError() {
                Toast.makeText(context, "Нет подключения к интернету", Toast.LENGTH_LONG).show();
            }
        });
    }

    public Float converterCurrency(Context context, String sumString,int idFirstCurrency, int idSecondCurrency) {
        final CurrencyDataBaseHelper dbHelper = new CurrencyDataBaseHelper(context);
        final CurrencyDataBaseManager currencyDataBaseManager = new CurrencyDataBaseManager(dbHelper);
        float sumFloat = Float.parseFloat(sumString);

        // проверка БД
        if (!currencyDataBaseManager.isEmpty()) {
            float firstCoeffCurrency = currencyDataBaseManager.getListCurrency().get(idFirstCurrency).getValue() / currencyDataBaseManager.getListCurrency().get(idFirstCurrency).getNominal();
            float secondCoeffCurrency = currencyDataBaseManager.getListCurrency().get(idSecondCurrency).getValue() / currencyDataBaseManager.getListCurrency().get(idSecondCurrency).getNominal();
            float finalCoeffCurrency = firstCoeffCurrency / secondCoeffCurrency;
            return sumFloat * finalCoeffCurrency;
        } else {
            Toast.makeText(context, "Данные отсутствуют", Toast.LENGTH_LONG).show();
        }
        return 0f;
    }
}
