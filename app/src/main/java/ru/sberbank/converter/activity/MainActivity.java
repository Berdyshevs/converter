package ru.sberbank.converter.activity;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import ru.sberbank.converter.R;
import ru.sberbank.converter.model.CurrencyManager;

public class MainActivity extends AppCompatActivity {

    private int firstCoeffCurrency;
    private int secondCoeffCurrency;
    private Button buttonConvert;
    private EditText editTextInputSum;
    private EditText editTextOutputSum;
    private Spinner spinnerInputCurrency;
    private Spinner spinnerOutputCurrency;
    private CurrencyManager currencyManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dataDownload();
        init();
        spinnerDefault();
    }

    /**
     * Метод задает стартовые валюты
     */
    private void spinnerDefault() {
        spinnerInputCurrency.setSelection(11);
        spinnerOutputCurrency.setSelection(0);
    }

    /**
     * Метод загружает данные о курсе валют
     */
    private void dataDownload() {
        currencyManager = new CurrencyManager();
        currencyManager.getDataCurrency(this);
    }

    private void init() {
        editTextInputSum = findViewById(R.id.etInputSum);
        editTextOutputSum = findViewById(R.id.etOutputSum);

        buttonConvert = findViewById(R.id.btnConvert);
        buttonConvert.setOnClickListener(buttonConvertClickListener);

        spinnerInputCurrency = findViewById(R.id.spInput);
        spinnerInputCurrency.setOnItemSelectedListener(spinnerInputCurrencyItemSelectedListener);

        spinnerOutputCurrency = findViewById(R.id.spOutput);
        spinnerOutputCurrency.setOnItemSelectedListener(spinnerOutputCurrencyItemSelectedListener);
    }

    private View.OnClickListener buttonConvertClickListener = new View.OnClickListener() {
        @SuppressLint("SetTextI18n")
        @Override
        public void onClick(View v) {
            editTextOutputSum.setText(currencyManager.converterCurrency(MainActivity.this,
                    editTextInputSum.getText().toString(),
                    firstCoeffCurrency, secondCoeffCurrency).toString());
        }
    };

    private AdapterView.OnItemSelectedListener spinnerInputCurrencyItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            firstCoeffCurrency = position;
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    private AdapterView.OnItemSelectedListener spinnerOutputCurrencyItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            secondCoeffCurrency = position;
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };
}
